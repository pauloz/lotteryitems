﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LotteryItems.Models;
using LotteryItems.Controllers;

namespace LotteryItems.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewItemPage : ContentPage
    {
        public Item Item { get; set; }

        public NewItemPage()
        {
            InitializeComponent();

            Item = new Item
            {
                Name = "",
                Description = ""
            };

            BindingContext = this;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            var itemController = new ItemController();
            await itemController.Save(Item);

            //MessagingCenter.Send(this, "AddItem", Item);
            await Navigation.PopModalAsync();
        }
    }
}