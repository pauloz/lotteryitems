﻿using LotteryItems.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LotteryItems.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Lottery : ContentPage
	{
        LotteryViewModel viewModel;

        public Lottery ()
		{
			InitializeComponent ();

            BindingContext = viewModel = new LotteryViewModel();
                       
        }
	}
}