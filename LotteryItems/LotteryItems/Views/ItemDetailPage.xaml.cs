﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using LotteryItems.Models;
using LotteryItems.ViewModels;
using LotteryItems.Controllers;

namespace LotteryItems.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemDetailPage : ContentPage
	{
        ItemDetailViewModel viewModel;
        public Item Item { get; set; }

        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();

            this.Item = viewModel.Item;
            BindingContext = this;
            OnPropertyChanged();
        }

        public ItemDetailPage()
        {
            InitializeComponent();

            Item = new Item
            {
                Name = "Nome do item",
                Description = "Descrição do item"
            };

            BindingContext = this;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            var itemController = new ItemController();
            await itemController.Save(Item);

            await Navigation.PushAsync(new ItemsPage());
        }

        async void Delete_Clicked(object sender, EventArgs e)
        {
            var itemController = new ItemController();
            await itemController.Delete(Item);

            await Navigation.PushAsync(new ItemsPage());
        }
    }
}