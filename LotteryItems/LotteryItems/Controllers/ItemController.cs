﻿using LotteryItems.Entities;
using LotteryItems.Models;

namespace LotteryItems.Controllers
{
    public class ItemController : EntityController<Item>
    {
        public ItemController() : base(App.SqlConnection)
        {
            App.SqlConnection.CreateTableAsync<Item>();
        }
        public void ResetDatabase()
        {
            App.SqlConnection.DropTableAsync<Item>();
            App.SqlConnection.CreateTableAsync<Item>();
        }
    }
}
