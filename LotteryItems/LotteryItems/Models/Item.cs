﻿namespace LotteryItems.Models
{
    public class Item : Entity
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (value.Equals(_name))
                {
                    return;
                }
                _name = value;
                OnPropertyChanged();
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                if (value.Equals(_description))
                {
                    return;
                }
                _description = value;
                OnPropertyChanged();
            }
        }
    }
}