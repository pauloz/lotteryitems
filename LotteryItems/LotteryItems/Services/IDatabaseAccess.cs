﻿using SQLite;

namespace LotteryItems.Services
{
    public interface IDatabaseAccess
    {
        SQLiteAsyncConnection GetConnection();
    }
}
