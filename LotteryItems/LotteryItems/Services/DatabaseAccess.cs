﻿using LotteryItems.Services;
using SQLite;
using System;
using Xamarin.Forms;

[assembly: Dependency(typeof(DatabaseAccess))]
namespace LotteryItems.Services
{
    public class DatabaseAccess : IDatabaseAccess
    {
        public DatabaseAccess()
        {

        }

        public SQLiteAsyncConnection GetConnection()
        {
            var sqlDbFileName = "lotteryitems.db3";
            var documentsPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = System.IO.Path.Combine(documentsPath, sqlDbFileName);

            var connection = new SQLiteAsyncConnection(path);
            
            return connection;
        }
    }
}
