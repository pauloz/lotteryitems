﻿using LotteryItems.Services;
using LotteryItems.Views;
using SQLite;
using Xamarin.Forms;

namespace LotteryItems
{
    public partial class App : Application
    {
        public static SQLiteAsyncConnection SqlConnection;
        public static DatabaseAccess dbAccess;

        public App()
        {
            InitializeComponent();
            MainPage = new MainPage();

            dbAccess = new DatabaseAccess();
            SqlConnection = dbAccess.GetConnection();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
