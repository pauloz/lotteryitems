﻿using System;
using System.Windows.Input;

using Xamarin.Forms;

namespace LotteryItems.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "Sobre";

            OpenWebCommand = new Command(() => Device.OpenUri(new Uri("https://www.linkedin.com/in/paulozuchini/")));
        }

        public ICommand OpenWebCommand { get; }
    }
}