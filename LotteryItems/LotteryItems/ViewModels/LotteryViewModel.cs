﻿using LotteryItems.Controllers;
using LotteryItems.Models;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;

namespace LotteryItems.ViewModels
{
    public class LotteryViewModel : BaseViewModel
    {
        public ObservableCollection<Item> Items { get; set; }
        public Item SortedItem { get; set; }
        public ICommand LotteryCommand { get; }
        public string History { get; set; }

        public LotteryViewModel()
        {
            Title = "Lottery";

            History = "";

            LotteryCommand = new Command(async () => await SortItemsCommandAsync());
        }

        async Task SortItemsCommandAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items = new ObservableCollection<Item>();
                var itemController = new ItemController();
                var ItemsList = await itemController.Get();
                Items = new ObservableCollection<Item>(ItemsList);

                Random rnd = new Random();
                int r = rnd.Next(Items.Count);

                SortedItem = Items[r];

                History += SortedItem.Name + Environment.NewLine;

                OnPropertyChanged("History");
            }

            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

    }
}