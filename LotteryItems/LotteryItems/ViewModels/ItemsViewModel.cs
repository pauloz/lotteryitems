﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Xamarin.Forms;
using LotteryItems.Models;
using LotteryItems.Controllers;
using System.Threading.Tasks;

namespace LotteryItems.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        public ObservableCollection<Item> Items { get; set; }
        public Command LoadItemsCommand { get; set; }
        public Command DeleteAllCommand { get; set; }

        public ItemsViewModel()
        {
            Title = "Lista";
            Items = new ObservableCollection<Item>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
            DeleteAllCommand = new Command(() => ExecuteDeleteAllCommand());

            //MessagingCenter.Subscribe<NewItemPage, Item>(this, "AddItem", async (obj, item) =>
            //{
            //    var _item = item as Item;
            //    Items.Add(_item);
            //    await DataStore.AddItemAsync(_item);
            //});
        }

        void ExecuteDeleteAllCommand()
        {
            var itemController = new ItemController();
            itemController.ResetDatabase();
            Items.Clear();
            OnPropertyChanged("Items");
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var itemController = new ItemController();
                var ItemsList = await itemController.Get();
                Items = new ObservableCollection<Item>(ItemsList);
                OnPropertyChanged("Items");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}